#include "plateau.h"
#ifndef MIN_MAX_H_INCLUDED
#define MIN_MAX_H_INCLUDED

///Classe score qui comprend les coordonnées et le score elle nous est essentielle au fonctionnement de min_max

class Score
{
public:
    pair<int,int> m_coordonnees;
    int m_score;
};

///Fonctions de l'algorithme minmax

Score max(int joueur, Plateau p, int profondeur);
Score min(int joueur, Plateau p, int profondeur);

#endif // MIN_MAX_H_INCLUDED
