#ifndef ARBRE_H_INCLUDED
#define ARBRE_H_INCLUDED

#include "plateau.h"
#include "console.h"
#include <windows.h>
#include <cstdlib>
#include <time.h>
#include <fstream>
#include "min_max.h"

typedef struct Noeud Noeud;
///Classe qui nous sert a creer l'arbre pour le niveau 2 il comprend la profondeur afin que l'on puisse determiner les parents a partir de la profondeur de recherche ainsi que le score et les coordonnees pour justifier le bon fonctionnement de min_max

struct Noeud
{
    pair<int,int> m_coordonnees;
    int m_score;
    int m_profondeur;
    pair<int,int> m_parent;
    ///Constructeur
    Noeud(int _x, int _y, int _score, int _profondeur);
};

#endif // ARBRE_H_INCLUDED
