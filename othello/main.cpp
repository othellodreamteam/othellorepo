#include <iostream>
#include <windows.h>
#include "console.h"
#include "plateau.h"
#include "min_max.h"

/*! \mainpage Jeu Othello v1.0 pour l'ECE Paris
 *
 * \section intro_sec Presentation
 *
 * Voici le projet du second semestre d'ING2 programme par l'equipe 3 du TD12
 * \date 24 avril 2017
 *
 *
 * \section install_sec Membres de l'equipe
 * Aurelien Coudert - Estelle Favreau - Valentin Leroy.
 *
 */

/*! \file min_max.h
 *  \section intro_sec Presentation
 *   Ce fichier nous sert a declarer les variables et a utiliser l'algorithme minmax.
 *  \author Aurelien Coudert - Estelle Favreau - Valentin Leroy.
 *
 */
/*! \file plateau.h
 *  \section intro_sec Presentation
 *   Ce fichier contient la structure du plateau
 *  \author Aurelien Coudert - Estelle Favreau - Valentin Leroy.
 *
 */
 /*! \file arbre.h
 *  \section intro_sec Presentation
 *   Ce fichier nous sert a declarer la structure noeud que nous utilisons pour présenter l'arbre
 *  \author Aurelien Coudert - Estelle Favreau - Valentin Leroy.
 *
 */


using namespace std;

int main()
{
    Plateau p;
    p.menu();

    return 0;
}
