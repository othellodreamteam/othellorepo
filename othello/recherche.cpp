#include "plateau.h"
#include "console.h"
#include <windows.h>
#include <cstdlib>
#include <time.h>
#include <fstream>
#include "min_max.h"

///Integre toutes les fonctions en dessous et retourne tous les pions a retourner ainsi que la validit� du coup
Recherche Plateau::autoriser_position(int nomDuJoueur, int pos_x, int pos_y)
{
    //Entier pour savoir qui ne joue pas, il va servir de comparaison aux cases du plateau
    int compJ;
    Recherche autorisation;
    Recherche OK;
    pair<int,int> coord ({pos_x,pos_y});
    autorisation.m_coord = coord;
    autorisation.m_valide=false;
    //Si c'est J1 qui joue
    if(nomDuJoueur == 1)
    {
        //On va chercher les pions j2
        compJ = 2;
    }
    // A l'inverse si J2 joue
    if(nomDuJoueur == 2)
    {
        //On va chercher les pions J1
        compJ = 1;
    }
    //Si on est dans le tableau et que la case d'apr�s est un pion adverse on rentre dans la condition
    if(m_plateau[pos_x][pos_y]==0)
    {
        if((pos_x-1)>0 && m_plateau[pos_x-1][pos_y]== compJ)
        {
            OK = this->recherche_haut(nomDuJoueur, compJ, pos_x, pos_y);
            if(OK.m_valide==true)
            {
                autorisation.m_valide=true;
                (autorisation.m_aRetourner).insert(autorisation.m_aRetourner.end(),OK.m_aRetourner.begin(),OK.m_aRetourner.end());
            }
        }
        //Recherche en bas
        if((pos_x+1) < 7 && m_plateau[pos_x+1][pos_y]== compJ)
        {
            OK = this->recherche_bas(nomDuJoueur, compJ, pos_x, pos_y);
            if(OK.m_valide==true)
            {
                autorisation.m_valide=true;
                (autorisation.m_aRetourner).insert(autorisation.m_aRetourner.end(),OK.m_aRetourner.begin(),OK.m_aRetourner.end());
            }
        }
        //Recherche a gauche
        if((pos_y-1)>0 && m_plateau[pos_x][pos_y-1]== compJ)
        {
            OK = this->recherche_gauche(nomDuJoueur, compJ, pos_x, pos_y);
            if(OK.m_valide==true)
            {
                autorisation.m_valide=true;
                (autorisation.m_aRetourner).insert(autorisation.m_aRetourner.end(),OK.m_aRetourner.begin(),OK.m_aRetourner.end());
            }
        }
        //recherche � droite
        if((pos_y+1)<7 && m_plateau[pos_x][pos_y+1]== compJ)
        {
            OK = this->recherche_droite(nomDuJoueur, compJ, pos_x, pos_y);
            if(OK.m_valide==true)
            {
                autorisation.m_valide=true;
                (autorisation.m_aRetourner).insert(autorisation.m_aRetourner.end(),OK.m_aRetourner.begin(),OK.m_aRetourner.end());
            }
        }
        if (( ((pos_y+1)<7)&&((pos_x+1)<7)) && m_plateau[pos_x+1][pos_y+1]==compJ)
        {
            //Diagonale Bas Droite
            OK = this->recherche_diagonale_bas_droite(nomDuJoueur, compJ, pos_x, pos_y);
            if(OK.m_valide==true)
            {
                autorisation.m_valide=true;
                (autorisation.m_aRetourner).insert(autorisation.m_aRetourner.end(),OK.m_aRetourner.begin(),OK.m_aRetourner.end());
            }
        }

        if (( ((pos_y-1)>0)&&((pos_x+1)<7)) && m_plateau[pos_x+1][pos_y-1]==compJ)
        {
            //Diagonale bas gauche
            OK = this->recherche_diagonale_bas_gauche(nomDuJoueur, compJ, pos_x, pos_y);
            if(OK.m_valide==true)
            {
                autorisation.m_valide=true;
                (autorisation.m_aRetourner).insert(autorisation.m_aRetourner.end(),OK.m_aRetourner.begin(),OK.m_aRetourner.end());
            }
        }

        if (( ((pos_y-1)>0)&&((pos_x-1)>0)) && m_plateau[pos_x-1][pos_y-1]==compJ)
        {
            //DHG
            OK = this->recherche_diagonale_haut_gauche(nomDuJoueur, compJ, pos_x, pos_y);
            if(OK.m_valide==true)
            {
                autorisation.m_valide=true;
                (autorisation.m_aRetourner).insert(autorisation.m_aRetourner.end(),OK.m_aRetourner.begin(),OK.m_aRetourner.end());
            }
        }

        if (( ((pos_y+1)<7)&&((pos_x-1)>0)) && m_plateau[pos_x-1][pos_y+1]==compJ)
        {
            //DHD
            OK = this->recherche_diagonale_haut_droite(nomDuJoueur, compJ, pos_x, pos_y);
            if(OK.m_valide==true)
            {
                autorisation.m_valide=true;
                (autorisation.m_aRetourner).insert(autorisation.m_aRetourner.end(),OK.m_aRetourner.begin(),OK.m_aRetourner.end());
            }
        }
    }
    //On retourne l'autorisaation de poser le pion � la case souhait�e
    return autorisation;
}

///Recherche tous les pions a retourner au dessus s'ils existent et retourne le tableau des pions a retourner
Recherche Plateau::recherche_haut(int nomDuJoueur, int compJ, int pos_x, int pos_y)
{

    Recherche a;
    int comp_x = pos_x;
    int comp_y = pos_y;
    //On parcours vers le haut
    while((comp_x-1) >0 && m_plateau[comp_x-1][comp_y]== compJ)
    {
        comp_x --;
        a.m_aRetourner.push_back({comp_x,comp_y});
    }

    //Si la case � gauche est vide, il ne se passe rien
    if(m_plateau[comp_x-1][comp_y]==0)
    {
        a.m_valide=false;
        return a;
    }
    //si on sort tu plateau

    else if((comp_x-1) < 0 )
    {
        a.m_valide=false;
        return a;
    }
    //si c'est un pion ali�,
    else if(m_plateau[comp_x-1][comp_y] == nomDuJoueur)
    {
        a.m_valide=true;
        return a;
    }

    else
    {
        a.m_valide=false;
        return a;
    }
}

///Recherche tous les pions a retrouner en bas s'ils existent et retourne le tableau des pions a retourner
Recherche Plateau::recherche_bas(int nomDuJoueur, int compJ, int pos_x, int pos_y)
{

    Recherche a;
    int comp_x = pos_x;
    int comp_y = pos_y;
    //On parcours vers le bas
    while((comp_x+1) <7 && m_plateau[comp_x+1][comp_y]== compJ)
    {
        comp_x ++;
        a.m_aRetourner.push_back({comp_x,comp_y});
    }

    //Si la case en bas est vide, il ne se passe rien
    if(m_plateau[comp_x+1][comp_y]==0)
    {
        a.m_valide=false;
        return a;
    }
    //si on sort tu plateau

    else if((comp_x+1) > 7 )
    {
        a.m_valide=false;
        return a;
    }
    //si c'est un pion ali�,
    else if(m_plateau[comp_x+1][comp_y] == nomDuJoueur)
    {
        a.m_valide=true;
        return a;
    }
    else
        a.m_valide=false;
    return a;
}

///Recherche tous les pions a retrourner a gauche s'ils existent et retourne le tableau des pions a retourner
Recherche Plateau::recherche_gauche(int nomDuJoueur, int compJ, int pos_x, int pos_y)
{

    Recherche a;
    int comp_x = pos_x;
    int comp_y = pos_y;
    //On parcours vers le haut
    while((comp_y-1) >0 && m_plateau[comp_x][comp_y-1]== compJ)
    {
        comp_y --;
        a.m_aRetourner.push_back({comp_x,comp_y});
    }

    //Si la case � gauche est vide, il ne se passe rien
    if(m_plateau[comp_x][comp_y-1]==0)
    {
        a.m_valide=false;
        return a;
    }
    //si on sort tu plateau

    else if((comp_y-1) < 0 )
    {
        a.m_valide=false;
        return a;
    }
    //si c'est un pion ali�,
    else if(m_plateau[comp_x][comp_y-1] == nomDuJoueur)
    {
        a.m_valide=true;
        return a;
    }

    else
    {
        a.m_valide=false;
        return a;
    }
}

///Recherche tous les pions a droite et retourne le tableau des pions a retourner
Recherche Plateau::recherche_droite(int nomDuJoueur, int compJ, int pos_x, int pos_y)
{

    int comp_x = pos_x;
    Recherche a;
    int comp_y = pos_y;
    //On parcours vers le haut
    while((comp_y+1) < 7 && m_plateau[comp_x][comp_y+1]== compJ)
    {
        comp_y ++;
        a.m_aRetourner.push_back({comp_x,comp_y});

    }

    //Si la case � gauche est vide, il ne se passe rien
    if(m_plateau[comp_x][comp_y+1]==0)
    {
        a.m_valide=false;
        return a;
    }
    //si on sort tu plateau
    else if((comp_y+1) > 7 )
    {
        a.m_valide=false;
        return a;
    }
    //si c'est un pion ali�,
    else if(m_plateau[comp_x][comp_y+1] == nomDuJoueur)
    {
        a.m_valide=true;
        return a;
    }

    else
    {
        a.m_valide=false;
        return a;
    }
}


///Recherche tous les pions en diagonale s'ils existent et retourne le tableau des pions a retourner
Recherche Plateau::recherche_diagonale_bas_droite(int nomDuJoueur, int compJ, int pos_x, int pos_y)
{
    int comp_x = pos_x;
    Recherche a;
    int comp_y = pos_y;
    //parcours en diagonale bas droite
    while( ( ((comp_x+1)<7) &&(comp_y+1)<7) && m_plateau[comp_x+1][comp_y+1]==compJ )
    {
        comp_x++;
        comp_y++;
        a.m_aRetourner.push_back({comp_x,comp_y});
    }
    if(m_plateau[comp_x+1][comp_y+1]==0 || ((comp_x+1)&&(comp_y+1))>7)
    {
        a.m_valide=false;
        return a;
    }
    else if (m_plateau[comp_x+1][comp_y+1]==nomDuJoueur)
    {
        a.m_valide=true;
        return a;
    }
    else
    {
        a.m_valide=false;
        return a;
    }
}

///Recherche tous les pions en diagonale s'ils existent et retourne le tableau des pions a retourner
Recherche Plateau::recherche_diagonale_bas_gauche(int nomDuJoueur, int compJ, int pos_x, int pos_y)
{
    int comp_x = pos_x;
    Recherche a;
    int comp_y = pos_y;
    //parcours en diagonale bas droite
    while( ((comp_x+1)<7 && (comp_y-1)>0) && m_plateau[comp_x+1][comp_y-1]==compJ )
    {
        comp_x++;
        comp_y--;
        a.m_aRetourner.push_back({comp_x,comp_y});
    }
    if(m_plateau[comp_x+1][comp_y-1]==0 || ((comp_x+1)>7 && (comp_y-1)<0))
    {
        a.m_valide=false;
        return a;
    }
    if (m_plateau[comp_x+1][comp_y-1]==nomDuJoueur)
    {
        a.m_valide=true;
        return a;
    }
    else
    {
        a.m_valide=false;
        return a;
    }
}

///Recherche tous les pions en diagonale s'ils existent et retourne le tableau des pions a retourner
Recherche Plateau::recherche_diagonale_haut_gauche(int nomDuJoueur, int compJ, int pos_x, int pos_y)
{
    int comp_x = pos_x;
    Recherche a;
    int comp_y = pos_y;
    //parcours en diagonale bas droite
    while( ( ((comp_x-1)>0) &&(comp_y-1)>0) && m_plateau[comp_x-1][comp_y-1]==compJ )
    {
        comp_x--;
        comp_y--;
        a.m_aRetourner.push_back({comp_x,comp_y});
    }
    if((m_plateau[comp_x-1][comp_y-1] == 0) || ((comp_x-1)<0 &&(comp_y-1)<0))
    {
        a.m_valide=false;
        return a;
    }
    else if (m_plateau[comp_x-1][comp_y-1]==nomDuJoueur)
    {
        a.m_valide=true;
        return a;
    }
    else
    {
        a.m_valide=false;
        return a;
    }
}

///Recherche tous les pions en diagonale s'ils existent et retourne le tableau des pions a retourner
Recherche Plateau::recherche_diagonale_haut_droite(int nomDuJoueur, int compJ, int pos_x, int pos_y)
{
    int comp_x = pos_x;
    Recherche a;
    int comp_y = pos_y;
    //parcours en diagonale bas droite
    while( ((comp_x-1)>0 && (comp_y+1)<7) && m_plateau[comp_x-1][comp_y+1]==compJ )
    {
        comp_x--;
        comp_y++;
        a.m_aRetourner.push_back({comp_x,comp_y});
    }
    if(m_plateau[comp_x-1][comp_y+1]==0 || ((comp_x-1)<0 && (comp_y+1)>7))
    {
        a.m_valide=false;
        return a;
    }
    if (m_plateau[comp_x-1][comp_y+1]==nomDuJoueur)
    {
        a.m_valide=true;
        return a;
    }
    else
    {
        a.m_valide=false;
        return a;
    }
}
