#include "arbre.h"

Noeud::Noeud(int _x, int _y, int _score, int _profondeur)
{
    m_coordonnees.first = _x;
    m_coordonnees.second = _y;
    m_score = _score;
    m_profondeur = _profondeur;
}
