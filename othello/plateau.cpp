#include "plateau.h"
#include "console.h"
#include <windows.h>
#include <cstdlib>
#include <time.h>
#include <fstream>
#include <conio.h>
#include "min_max.h"
#include "arbre.h"


///Destructeur par d�faut
Plateau::~Plateau()
{
    //Rien � d�truire
}

///Constructeurs par d�faut
Plateau::Plateau()
{
    //Initialisation du tableau (double vecteur)
    vector< vector<int> > vec(8, vector<int>(8));
    m_plateau=vec;
    //initialisation de toutes les cases vide = 0
    for(int i=0; i<8; i++)
    {
        for(int j=0; j<8; j++)
        {
            m_plateau[i][j]=0;
        }
    }
    //Positionnement des points de d�part des 4 pions
    m_plateau[3][4]=2;
    m_plateau[3][3]=1;
    m_plateau[4][3]=2;
    m_plateau[4][4]=1;
}


///M�thode d'affichage du plateau en fonction du remplissage des cases
void Plateau::afficher_plateau()
{
    //Init des fonctions consoles
    Console* pConsole=NULL;
    char c('A');
    int v(1);
    pConsole=Console::getInstance();
    //Couleur en blanc pour les bordures et les cases vides
    pConsole->setColor(COLOR_WHITE);
    system("cls");
    //parcours du tableau
    for(int i=0;i<8;i++)
    {
        cout<<" "<<v;
        v++;
    }
    cout<<endl;
    for(int i=0; i<8; i++)
    {
        cout<<c;
        c++;
        for(int j=0; j<8; j++)
        {
            //Bordures
            if(m_plateau[i][j]==0)
            {
                pConsole->setColor(COLOR_RED_ON_GREEN);
                cout<<" |";
            }
            //pion joueur 1
            else if(m_plateau[i][j]==1)
            {
                pConsole->setColor(COLOR_WHITE_ON_GREEN);
                cout<<"X|";
            }
            //pion joueur 2
            else if(m_plateau[i][j]==2)
            {
                pConsole->setColor(COLOR_BLACK_ON_GREEN);
                cout<<"X|";
            }
            //Reset de la couleur en blanc
            pConsole->setColor(COLOR_WHITE);
        }
        cout<<"\n";
    }
    comptage_points();
}

///deplacement du curseur
void Plateau::selection_case(int* px, int* py)
{
    bool quit(false);
    int x(0),y(0);
    Console* pConsole=NULL;
    pConsole=Console::getInstance();
    while (!quit)
    {
        // Si on a appuy� sur une touche du clavier
        if (pConsole->isKeyboardPressed())
        {
            // R�cup�re le code ASCII de la touche avec blindage des bordures du tableau
            int key = getch();
            if(key==122|| key==72)
            {
                if (x>0)
                    x--;
            }
            else if(key==115 || key==80)
            {
                if (x<7)
                    x++;
            }
            else if (key==100 || key==77 )
            {
                if(y<7)
                    y++;
            }

            else if(key==75 || key==113)
            {
                if(y>0)
                    y--;
            }

            //positionnement pour l'affichage
            pConsole->gotoLigCol(3,42);
            //affichage des coordonn�es
            std::cout<<static_cast<char> (x+65)<<";"<<y+1<<";"<<m_plateau[x][y]<<key;
            //positionnement du curseur
            pConsole->gotoLigCol(x+1,2*y+1);
            if (key == 13) // 27 = touche escape
            {
                //on quitte la boucle
                quit = true;
                //On attribue les coordonn�es
                *px=x;
                *py=y;
                pConsole->gotoLigCol(7,0);
            }
        }
    }
}

///pose du pion sur la tableau
void Plateau::poser_pion(int joueur)
{
    //RES
    int x(0),y(0);
    Recherche test;
    test.m_valide=false;
    //Boucle de jeu
    while(test.m_valide == false)
    {
        //on selectionne la case
        this->selection_case(&x,&y);
        //on regarde si on peut poser le pion, si il y a un possibilit� de jouer
        test=this->autoriser_position(joueur,x,y);
        //on pose le pion
    }
    //Si le test est valide on pose un pion et on retourne tous les suivants
    if(test.m_valide==true)
    {
        m_plateau[x][y]=joueur;
        for(vector< pair<int,int> >::iterator it=test.m_aRetourner.begin(); it!=test.m_aRetourner.end(); it++)
        {
            pair<int,int> a=(*it);
            m_plateau[a.first][a.second]=joueur;
        }
    }
    //Si les conditions ne sont pas r�unies on cr�e de la recursivit�
    else
        this->poser_pion(joueur);
    //on affiche le nouveau tableau
    this->afficher_plateau();
}
///Pose le pion sur le plateau en ayant d�j� ses coordonn�es
void Plateau::poser_pion_avec_coord(int joueur,int ax, int ay)
{
    //RES
    int x(ax),y(ay);
    Recherche test;
    test.m_valide=false;
    //Boucle de jeu
    while(test.m_valide == false)
    {
        /*//on selectionne la case
        this->selection_case(&x,&y);
        //on regarde si on peut poser le pion, si il y a un possibilit� de jouer*/
        test=this->autoriser_position(joueur,x,y);
        //on pose le pion
    }
    //Si le test est valide on pose un pion et on retourne tous les suivants
    if(test.m_valide==true)
    {
        m_plateau[x][y]=joueur;
        for(vector< pair<int,int> >::iterator it=test.m_aRetourner.begin(); it!=test.m_aRetourner.end(); it++)
        {
            pair<int,int> a=(*it);
            m_plateau[a.first][a.second]=joueur;
        }
    }
    //Si les conditions ne sont pas r�unies on cr�e de la recursivit�
    else
        this->poser_pion(joueur);
    //on affiche le nouveau tableau
    //this->afficher_plateau();
}

///Push dans un vecteur de pair les posisitons jouables dans le tableau
vector<pair<int,int> > Plateau::positions_jouables(int joueur)
{
    //RES
    vector<pair<int,int> > positionJouables;
    positionJouables.clear();
    //on fait une boucle qui parcours tout le tableau en quete d'une position jouable
    for(int i = 0; i<m_plateau.size(); i++)
    {
        for(int j=0; j<m_plateau[i].size(); j++)
        {
            //on r�cup�re la case
            pair<int,int>  a ({i,j});
            //Si on peut poser notre pions sur la case
            if(this->autoriser_position(joueur,i,j).m_valide == true)
            {
                //On ajoute les coordonn�es dans le vecteur
                positionJouables.push_back(a);
            }
        }
    }

    return positionJouables;
}

///Afficher l'arbre pour le niveau 1
void Plateau::arbre_niveau1(vector<pair<int,int> > tab_pos, int joueur)
{
    Recherche recup;
    Console* pConsole=NULL;
    pConsole=Console::getInstance();
    int cpt=1;
    pConsole->gotoLigCol(10,0);
    cout << "Arbre des possibilites de jeu pour le joueur " << joueur << endl;
    for(int i=0; i < tab_pos.size(); i++)
    {
        recup = autoriser_position(joueur, tab_pos[i].first, tab_pos[i].second);

        cpt = recup.m_aRetourner.size();

        for(int i=0; i<m_plateau.size(); i++)
        {
            for(int j=0; j<m_plateau[i].size(); j++)
            {
                if(m_plateau[i][j]==joueur)
                    cpt++;
            }
        }
        cout << "Poss " << i+1 << "   Coordonnee : [" << tab_pos[i].first << ";" << tab_pos[i].second << "]  " << " Score : " << cpt << endl;
    }
}
///Pose al�atoirement le pion selon les possibilit�s de coups
void Plateau::jeu_aleatoire(int joueur)
{
    //RES et init
    int a;
    pair<int,int> coord_temp;
    vector<pair<int,int> > tab_pos;
    tab_pos.clear();
    srand(time(NULL));
    //On remplit notre tableau de coordoonn�es possibles
    tab_pos=this->positions_jouables(joueur);
    arbre_niveau1(tab_pos, joueur);
    system("pause");
    //On s�l�ctionne une case al�atoire du vecteur
    a=rand()%tab_pos.size();
    //On r�cup�re les coordoon�es de cette case
    coord_temp = (tab_pos[a]);
    //On pose le pion � la case souhait�e
    this->poser_pion_avec_coord(joueur, coord_temp.first, coord_temp.second);
}

///V�rification des posisitons jouabes
bool Plateau::fin_du_jeu(int joueur)
{
    vector<pair<int,int> > tab_pos;
    //on regarde s'il reste des posisitions jouables
    tab_pos=this->positions_jouables(joueur);
    //Si on ne peut pas jouer
    if(tab_pos.empty())
        return true;
    //Sinon on peut jouer
    else
        return false;
}

///Parcours du tableau et comptage des points
void Plateau::comptage_points()
{
    Console* pConsole=NULL;
    pConsole=Console::getInstance();
    int cpt1(0), cpt2(0);
    for(int i=0; i<m_plateau.size(); i++)
    {
        for(int j=0; j<m_plateau[i].size(); j++)
        {
            if(m_plateau[i][j]==1)
                cpt1++;
            else if(m_plateau[i][j]==2)
                cpt2++;
        }
    }
    pConsole->gotoLigCol(4,25);
    cout<<"Score du joueur 1 : "<<cpt1<<"\tScore du joueur 2 : "<<cpt2;
    pConsole->gotoLigCol(10,0);
}

///Parcours du tableau et Calcul du score joueur
int Plateau::score_joueur(int joueur)
{
    int score(0);
    for(int i=0; i<m_plateau.size(); i++)
        for(int j=0; j<m_plateau[i].size(); j++)
        {
            if(m_plateau[i][j]==joueur)
                score++;
        }
    return score;
}

///Retourne le score du joueur a l'aide de notre carte heuristique
int Plateau::score_joueur_heuristique(int joueur)
{
    ifstream fichier("heuristique.txt");
    int plat[8][8];
    int score(0);
    for(int i=0; i<m_plateau.size(); i++)
        for(int j=0; j<m_plateau[i].size(); j++)
        {
            fichier>>plat[i][j];
            if(m_plateau[i][j]==1)
                score+=plat[i][j];
        }

    return score;
}

///comptage des scores et affichage des r�sultats pauses et clear
void Plateau::comptage_points_final()
{
    Console* pConsole=NULL;
    pConsole=Console::getInstance();
    int cpt1(0), cpt2(0);
    //Parcours du plateau
    for(int i=0; i<m_plateau.size(); i++)
    {
        for(int j=0; j<m_plateau[i].size(); j++)
        {
            //On compte les scores
            if(m_plateau[i][j]==1)
                cpt1++;
            else if(m_plateau[i][j]==2)
                cpt2++;
        }
    }
    system("cls");
    //Si joueur 1 gagne
    if(cpt1 > cpt2)
    {
        cout << "Victoire J1 avec "<<cpt1 <<" points !"<<endl;
        cout << "J2 n'est pas loin derriere avec un score de "<< cpt2 << endl;
        cout <<"Bravo !" <<endl;
    }
    //Si joueur 2 gagne
    else if(cpt1 < cpt2)
    {
        cout << "Victoire J2 avec "<<cpt2 <<" points !"<<endl;
        cout << "J1 n'est pas loin derriere avec un score de "<< cpt1 << endl;
        cout <<"Bravo !" <<endl;
    }
    //Si il y a �galit�
    else if(cpt1 == cpt2)
    {
        cout << "Egalit� parfaite !"<<endl;
        cout << "Vous avez tous les deux "<< cpt1<<" points." << endl;
        cout <<"Bravo !" <<endl;
    }
    //On pause sur l'affichage pour pouvoir lire
    system("pause");
    this->menu();
}

///Menu du jeu
void Plateau::menu()
{
    //RES
    char choix;
    bool retour_menu(false);
    Plateau p;

    //Do while pour revenir au menu meme � la fin d'une partie
    do
    {
        //affichage des options
        system("cls");

        Console* pConsole=NULL;
        pConsole->setColor(COLOR_RED);

        std::cout<<std::endl;
        std:cout<<std::endl;
        std::cout<<"               ***************************************"<<std::endl;
        std::cout<<"               *              Othello                *"<<std::endl;
        std::cout<<"               *                                     *"<<std::endl;
        std::cout<<"               *      1- Multi-joueur 1vs1           *"<<std::endl;
        std::cout<<"               *      2- Niveau 1                    *"<<std::endl;
        std::cout<<"               *      3- Niveau 2                    *"<<std::endl;
        std::cout<<"               *      4- Charger une partie          *"<<std::endl;
        std::cout<<"               *      5- Regle du jeu                *"<<std::endl;
        std::cout<<"               *      6- Quitter                     *"<<std::endl;
        std::cout<<"               *                                     *"<<std::endl;
        std::cout<<"               ***************************************"<<std::endl;

        //Recup�ration du choix
        std::cin>>choix;
        //Switch pour le cas par cas
        switch(choix)
        {
        //1v1 contre joueurs
        case '1':
        {
            //res & init
            Console* pConsole=NULL;
            pConsole=Console::getInstance();
            pConsole->setColor(COLOR_WHITE);
            p.afficher_plateau();
            //Tant que le plateau n'est pas plein
            while(p.fin_du_jeu(1)== false && p.fin_du_jeu(2)==false)
            {
                //sauvegarde a chaque tour de jeu la partie dans un fichier
                p.poser_pion(1);
                p.sauvegarde();
                p.poser_pion(2);
                p.sauvegarde();
            }
            p.comptage_points_final();

        }
        break;
        case '2':
        {
            // Niveau 1
            Console* pConsole=NULL;
            // int x,y;
            pConsole=Console::getInstance();
            pConsole->setColor(COLOR_WHITE);

            p.afficher_plateau();
            while(p.fin_du_jeu(1)== false && p.fin_du_jeu(2)==false)
            {
                p.jeu_aleatoire(1);
                p.afficher_plateau();
                p.sauvegarde();
                p.positions_jouables(1);
                if(p.fin_du_jeu(1))
                    break;
                system("pause");
                p.jeu_aleatoire(2);
                p.afficher_plateau();
                p.positions_jouables(2);
                if(p.fin_du_jeu(2))
                    break;
                system("pause");
            }
            p.comptage_points_final();
        }
        break;
        case '3':
        {
            // Niveau 2
            Console* pConsole=NULL;
            pConsole=Console::getInstance();
            pConsole->setColor(COLOR_WHITE);

            Score no_options_available;
            no_options_available.m_coordonnees.first=-1;
            no_options_available.m_coordonnees.second=-1;
            no_options_available.m_score=-1;
            p.afficher_plateau();
            int cpt(0);
            while(1)
            {
debut:
                if(cpt==2)
                    break;
                p.poser_pion(1);
                if(p.fin_du_jeu(1))
                    {
                        cpt++;
                        goto pt2;
                    }
                cpt=0;
                p.afficher_plateau();

                // sauvegarde la partie dans un fichier � chaque tour de jeu
                p.sauvegarde();
                if(p.fin_du_jeu(1))
                    break;
                //system("pause");
pt2:
                if(cpt==2)
                    break;
                Score s = max(2, p, 5);
                if(s.m_score==no_options_available.m_score && (s.m_coordonnees.first == no_options_available.m_coordonnees.first) && s.m_coordonnees.second == no_options_available.m_coordonnees.second)
                {
                    cpt++;
                    goto  debut;
                    break;
                }
                arbre_niveau2();
                system("pause");
                p.poser_pion_avec_coord(2, s.m_coordonnees.first, s.m_coordonnees.second);
                cpt=0;
                p.afficher_plateau();
                p.sauvegarde();
                if(p.fin_du_jeu(2))
                    break;
                //system("pause");
            }
            p.comptage_points_final();

        }
        break;

        case '4':
        {
            char c;
            p.charger();
            system("cls");
            std::cout << "        Chargement de partie"<<std::endl;
            std::cout<<""<<std::endl;
            std::cout<< "1- Multi-joueur 1vs1"<<std::endl;
            std::cout<< "2- Niveau 1 "<<std::endl;
            std::cout<< "3- Niveau 2 "<<std::endl;
            do
            {
                cin>>c;
            }
            while(c!='1'&&c!='2'&&c!='3');
            switch(c)
            {
            case '1':
            {
                //res & init
                Console* pConsole=NULL;
                pConsole=Console::getInstance();
                pConsole->setColor(COLOR_WHITE);
                p.afficher_plateau();
                //Tant que le plateau n'est pas plein
                while(p.fin_du_jeu(1)== false && p.fin_du_jeu(2)==false)
                {
                    p.poser_pion(1);
                    p.sauvegarde();
                    p.poser_pion(2);
                    p.sauvegarde();
                }
                p.comptage_points_final();

            }
            break;
            case '2':
            {
                // Niveau 1
                Console* pConsole=NULL;
                // int x,y;
                pConsole=Console::getInstance();
                pConsole->setColor(COLOR_WHITE);

                p.afficher_plateau();
                while(p.fin_du_jeu(1)== false && p.fin_du_jeu(2)==false)
                {
                    p.positions_jouables(1);
                    p.jeu_aleatoire(1);
                    p.afficher_plateau();
                    p.sauvegarde();
                    p.positions_jouables(1);
                    if(p.fin_du_jeu(1))
                        break;
                    system("pause");
                    p.positions_jouables(2);
                    p.jeu_aleatoire(2);
                    p.afficher_plateau();
                    p.positions_jouables(2);
                    if(p.fin_du_jeu(2))
                        break;
                    system("pause");
                }
                p.comptage_points_final();
            }
            break;
            case '3':
            {
                // Niveau 2
                Console* pConsole=NULL;
                pConsole=Console::getInstance();
                pConsole->setColor(COLOR_WHITE);

                Score no_options_available;
                no_options_available.m_coordonnees.first=-1;
                no_options_available.m_coordonnees.second=-1;
                no_options_available.m_score=-1;
                p.afficher_plateau();
                int cpt(0);
                while(1)
                {
debut1:

                    if(cpt==2)
                        break;
                    p.poser_pion(1);
                    if(p.fin_du_jeu(1))
                    {
                        cpt++;
                        goto pt21;
                    }
                    cpt=0;
                    p.afficher_plateau();
                    p.sauvegarde();
                    if(p.fin_du_jeu(1))
                        break;
                    //system("pause");
pt21:
                    if(cpt==2)
                        break;
                    Score s = max(2, p, 5);
                    if(s.m_score==no_options_available.m_score && (s.m_coordonnees.first == no_options_available.m_coordonnees.first) && s.m_coordonnees.second == no_options_available.m_coordonnees.second)
                    {
                        cpt++;
                        //retour au d�but
                        goto  debut1;
                        break;
                    }
                    cpt=0;
                    arbre_niveau2();
                    system("pause");
                    p.poser_pion_avec_coord(2, s.m_coordonnees.first, s.m_coordonnees.second);
                    p.afficher_plateau();
                    p.sauvegarde();
                    if(p.fin_du_jeu(2))
                        break;
                    //system("pause");
                }
                p.comptage_points_final();

            }
            break;
            default:
                {

                }
            }
        }
        break;
        case '5':
        {
          system("cls");
          Console* pConsole=NULL;
          pConsole->setColor(COLOR_PURPLE);

          std::cout<<"          Les regles du jeu"<<std::endl;
          std::cout<<std::endl;
          std::cout<<"Le but du jeu"<<std::endl;
          std::cout<<"  Le but est d'avoir � la fin de la partie le plus de pion en sa possession"<<std::endl;
          std::cout<<std::endl;
          std::cout<<"Les commandes"<<std::endl;
          std::cout<<"  On utilse les touches directionnelles pour deplacer le curseur et la touche entree"<<std::endl;
          std::cout<<"  pour placer un pion"<<std::endl;
          std::cout<<std::endl;
          std::cout<<"Les conditions pour jouer"<<std::endl;
          std::cout<<"  Pour pouvoir placer un pion il faut obligatoirement retourner un pion du joueur adverse"<<std::endl;
          std::cout<<std::endl;
          std::cout<<"A chaque tour la partie est sauvegarde vous pouvez donc a tout moment reprendre une partie"<<std::endl;
          std::cout<<std::endl;
          system("pause");

        }
        break;
        case '6' :
        {
            break;
        }
        default:
            {

            }
        }
    }
    while(choix != '6');
}

///Sauvegarde de la partie a chaque tour
void Plateau::sauvegarde()
{
    int cpt(0);
    //Ouverture du fichier
    ofstream fichier("sauvegarde.txt");
    //Parcours du tableau
    for(int i=0; i<m_plateau.size(); i++)
        for(int j=0; j<m_plateau.size(); j++)
        {
            //on enregistre chaque case du plateau dans le fichier
            fichier<<m_plateau[i][j];
            if(m_plateau[i][j]!=0)
                cpt++;
        }
    //si le modulo des pions pos� est = 0 alors c'est au J1 de jouer
    if(cpt%2==0)
        fichier<<1;
        //Sinon c'est au J2
    else
        fichier<<2;
}

///Charge la carte si l'utilisateur le souhaite
void Plateau::charger()
{
    //ouverture du fichier
    ifstream fichier("sauvegarde.txt");
    //RES
    int k;
    int joueur;
    //On parcours le plateau
    for(int i=0; i<m_plateau.size(); i++)
        for(int j=0; j<m_plateau.size(); j++)
        {
            //On get chaque caract�re
            k=fichier.get();
            //On remplit le plateau selon le fichier
            m_plateau[i][j]=k-48;
        }
        //on r�cup�re le tour du joueur
    joueur=fichier.get()-48;
    //On affiche le plateau
    this->afficher_plateau();
    //On attend une commande de l'utilisateur
    system("pause");
}

///Affiche l'arbre pour l'IA niveau 2
void Plateau::arbre_niveau2()
{
    //Ouverture du fichier
    ifstream fichier("arbre.txt");
    string lin;
    //RES
    vector<Noeud> tab;
    int recup_x, recup_y, recup_sco, recup_prof;

    if(fichier)
    {
        //On parcours le fichier et on recup�re toutes les donn�es dans l'ordre
        while(!fichier.eof())
        {
            //coordonnes puis score puis profondeur
            fichier >> recup_x >> recup_y >> recup_sco >> recup_prof;
            Noeud recup(recup_x, recup_y, recup_sco, recup_prof);
            //On met ca dans un vecteur
            tab.push_back(recup);
        }
        //On parcours le vecteur, on affiche d'abord en profondeur 5 puis 4 3 2 et 1
        cout<<"Profondeur 5 :"<<endl;
        for(int i=0; i < tab.size(); i++)
        {
            if(tab[i].m_profondeur == 5)
            {
                cout << "[" << tab[i].m_coordonnees.first << "," << tab[i].m_coordonnees.second << "]  Score : " << tab[i].m_score << "\t";
            }
        }
        cout<<"\n";
        cout<<"Profondeur 4 :"<<endl;
        for(int i=0; i < tab.size(); i++)
        {
            if(tab[i].m_profondeur == 4)
            {
                cout << "[" << tab[i].m_coordonnees.first << "," << tab[i].m_coordonnees.second << "]  Score : " << tab[i].m_score << "\t";
            }
        }
        cout<<"\n";
        cout<<"Profondeur 3 :"<<endl;
        for(int i=0; i < tab.size(); i++)
        {
            if(tab[i].m_profondeur == 3)
            {
                cout << "[" << tab[i].m_coordonnees.first << "," << tab[i].m_coordonnees.second << "]  Score : " << tab[i].m_score << "\t";
            }
        }
        cout<<"\n";
       /* for(int i=0; i < tab.size(); i++)
        {
            if(tab[i].m_profondeur == 2)
            {
                cout << "Profondeur 2: [" << tab[i].m_coordonnees.first << "," << tab[i].m_coordonnees.second << "]  Score : " << tab[i].m_score << endl;
            }
        }
        for(int i=0; i < tab.size(); i++)
        {
            if(tab[i].m_profondeur == 1)
            {
                cout << "Profondeur 1: [" << tab[i].m_coordonnees.first << "," << tab[i].m_coordonnees.second << "]  Score : " << tab[i].m_score << endl;
            }

            greagea
        }
        */
    }
}
