#include "min_max.h"
#include <fstream>
#include <windows.h>

using namespace std;

Score max(int joueur, Plateau p, int profondeur)
{
    int compJ;
    ofstream fichier;
    if(profondeur!=5)
        fichier.open("arbre.txt", ios::app);
    else
        fichier.open("arbre.txt");
    //int cpt(0);
    ///stock les positions jouables au tour d'apres
    vector<pair<int,int> > posJouables_temp;
    //tab de score
    vector<Score> tab_score;
    //stock les positions jouables
    vector<pair<int,int> > positionsJouables = p.positions_jouables(joueur);
    //retour
    Score fin;
    //si j=1 compJ=2 sinon compJ=1
    joueur==1 ? compJ=2 : compJ=1;
    //On teste tous les coups possibles pour une profondeur de 1
    if(profondeur==1 || profondeur == 0)
    {
        //parcours des positions jouables
        for(vector<pair<int,int> >::iterator it=positionsJouables.begin(); it!=positionsJouables.end(); it++)
        {
            //Pour toutes les positions jouables on joue le pion dans un plateau temporaire
            Score s;
            Plateau temp=p;
            //on pose le pion avec la position d'apres avec une des positions jouables
            temp.poser_pion_avec_coord(joueur, (*it).first, (*it).second);
            //Si le coup n'est pas un coup final
            if(!temp.fin_du_jeu(joueur))
            {
                //On r�cupere le score et on le stock dans un tableau
                s.m_score=temp.score_joueur_heuristique(compJ);
                s.m_coordonnees = (*it);
                tab_score.push_back(s);
            }
        }
        //S'il n'y a pas de score r�cup�r� on fait un message d'erreur que l'on interpretera dans la boucle de jeu
        if(tab_score.empty())
        {
            fin.m_coordonnees.first=-1;
            fin.m_coordonnees.second=-1;
            fin.m_score=-1;
        }
        //On initialise le score a la premiere valeur du tableau
        else
            fin=tab_score[0];
    }
    //Si la profondeur n'est pas = 1
    else
    {
        for(vector<pair<int,int> >::iterator it=positionsJouables.begin(); it!=positionsJouables.end(); it++)
        {
            //On stocke tous les coups dans un tableau
            Score s;
            Plateau temp=p;
            temp.poser_pion_avec_coord(joueur, (*it).first, (*it).second);
            if(!temp.fin_du_jeu(joueur))
            {
                //le score prend la valeur de min avec une profondeur -1
                s=min(compJ, temp, profondeur-1);
                s.m_coordonnees = (*it);
                tab_score.push_back(s);
            }

        }
        //Si il n'y a pas de score
        if(tab_score.empty())
        {
            fin.m_coordonnees.first=-1;
            fin.m_coordonnees.second=-1;
            fin.m_score=-1;
        }
        else
            fin=tab_score[0];
    }
    //on r�cupere la valeur max du score
    for(vector<Score>::iterator it=tab_score.begin(); it!=tab_score.end(); it++)
    {
        fichier<<(*it).m_coordonnees.first<<"\t"<<(*it).m_coordonnees.second<<"\t"<<(*it).m_score<<"\t"<<profondeur<<"\n";
        if(fin.m_score<=(*it).m_score)
            fin=(*it);
    }
    //fichier<<fin.m_coordonnees.first<<" "<<fin.m_coordonnees.second<<" "<<fin.m_score<<" "<<profondeur<<"\n";
    fichier.close();
    return fin;
}

Score min(int joueur, Plateau p, int profondeur)
{
    int compJ;
    ofstream fichier;
    fichier.open("arbre.txt", ios::app);
    vector<pair<int,int> > posJouables_temp;
    vector<Score> tab_score;
    vector<pair<int,int> > positionsJouables = p.positions_jouables(joueur);
    Score fin;
    joueur==1 ? compJ=2 : compJ=1;
    //On teste tous les coups possibles pour une profondeur de 1
    if(profondeur==1 || profondeur == 0)
    {
        //Pour toutes les positions jouables on joue le pion dans un plateau temporaire
        for(vector<pair<int,int> >::iterator it=positionsJouables.begin(); it!=positionsJouables.end(); it++)
        {
            Score s;
            Plateau temp=p;
            temp.poser_pion_avec_coord(joueur, (*it).first, (*it).second);
            //Si le coup n'est pas un coup final
            if(!temp.fin_du_jeu(joueur))
            {
                //On r�cupere le coup et on le stocke dans un tableau
                s.m_score=temp.score_joueur_heuristique(compJ);
                s.m_coordonnees = (*it);
                tab_score.push_back(s);
            }
        }
        //si le tableau est vide on renvoie un message d'erreur qui sera interprete dans la boucle jeu
        if(tab_score.empty())
        {
            fin.m_coordonnees.first=-1;
            fin.m_coordonnees.second=-1;
            fin.m_score=-1;
        }
        //on initialise le score a la premiere valeur du tableau
        else
            fin=tab_score[0];
    }
    else
    {
        for(vector<pair<int,int> >::iterator it=positionsJouables.begin(); it!=positionsJouables.end(); it++)
        {
            Score s;
            Plateau temp=p;
            temp.poser_pion_avec_coord(joueur, (*it).first, (*it).second);
            if(!temp.fin_du_jeu(joueur))
            {
                //Recursivit� de lalgorithme avec profondeur -1
                s=max(compJ, temp, profondeur-1);
                s.m_coordonnees = (*it);
                tab_score.push_back(s);
            }
        }
        //Si le tableau est nul
        if(tab_score.empty())
        {
            fin.m_coordonnees.first=-1;
            fin.m_coordonnees.second=-1;
            fin.m_score=-1;
        }
        //on initialise le tableau a a premiere valeur
        else
            fin=tab_score[0];
    }
    //On r�cupere le score minimal et on le retourne
    for(vector<Score>::iterator it=tab_score.begin(); it!=tab_score.end(); it++)
    {
        Score s=(*it);
        fichier<<s.m_coordonnees.first<<"\t"<<s.m_coordonnees.second<<"\t"<<s.m_score<<" "<<profondeur<<"\n";
        if(fin.m_score>=(*it).m_score)
            fin=(*it);
    }
    //fichier<<fin.m_coordonnees.first<<" "<<fin.m_coordonnees.second<<" "<<fin.m_score<<" "<<profondeur<<"\n";
    fichier.close();
    return fin;
}
