#include <vector>
#include <iostream>
#include <set>
#ifndef PLATEAU_H_INCLUDED
#define PLATEAU_H_INCLUDED

using namespace std;

typedef struct Recherche Recherche;


///Structure que l'on utilise pour effectuer la recherche des coups jouables et les pions a retourner Nous avons d�cid� de ne pas l'utiliser sous forme de classe par souci de facilit� de programmation les probl�matiques de s�curit� n'�tant pas au programme en ING2


struct Recherche
{
    bool m_valide;
    pair<int,int> m_coord;
    vector< pair<int,int> > m_aRetourner;
};


///Classe principale de notre jeu elle comprend le plateau de jeu et tous les regles ainsi que l'intelligence articicielle de niveau 1


class Plateau
{
private:
    //tableau � deux dimensions
    vector< vector<int> > m_plateau;
public:
    //Constructeurs et destructeurs
    Plateau();
    ~Plateau();

    ///M�thodes

    void placer_pion(int x,int y);
    void afficher_plateau();
    void selection_case(int* px, int* py);
    void poser_pion(int joueur);
    void poser_pion_avec_coord(int joueur,int ax, int ay);
    vector<pair<int,int> > positions_jouables(int joueur);
    Recherche autoriser_position(int nomDuJoueur, int pos_x, int pos_y);
    Recherche recherche_haut(int nomDuJoueur, int compJ, int pos_x, int pos_y);
    Recherche recherche_bas(int nomDuJoueur, int compJ, int pos_x, int pos_y);
    Recherche recherche_gauche(int nomDuJoueur, int compJ, int pos_x, int pos_y);
    Recherche recherche_droite(int nomDuJoueur, int compJ, int pos_x, int pos_y);
    Recherche recherche_diagonale_bas_droite(int nomDuJoueur, int compJ, int pos_x, int pos_y);
    Recherche recherche_diagonale_bas_gauche(int nomDuJoueur, int compJ, int pos_x, int pos_y);
    Recherche recherche_diagonale_haut_gauche(int nomDuJoueur, int compJ, int pos_x, int pos_y);
    Recherche recherche_diagonale_haut_droite(int nomDuJoueur, int compJ, int pos_x, int pos_y);
    void jeu_aleatoire(int joueur);
    bool fin_du_jeu(int joueur);
    void comptage_points();
    void comptage_points_final();
    int score_joueur(int joueur);
    int score_joueur_heuristique(int joueur);
    void menu();
    void sauvegarde();
    void charger();
    void arbre_niveau1(vector<pair<int,int> > tab_pos, int joueur);
    void arbre_niveau2();

};

#endif // PLATEAU_H_INCLUDED
